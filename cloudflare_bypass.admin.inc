<?php

/**
 * @file
 * Admin UI forms.
 */

/**
 * Implements hook_form().
 */
function cloudflare_bypass_settings_form($form, &$form_state) {

  if (!cloudflare_bypass_check_auth()) {
    return;
  }

  $params = [
    'mode' => 'bypass',
    'notes' => cloudflare_bypass_api_rule_key(),
    'per_page' => 500,
  ];
  $response = cloudflare_bypass_api_list_rules($params);

  if (empty($response->result)) {
    $form['empty'] = array(
      '#markup' => '<p>' . t('There are no users bypassed at this time.') . '</p>',
    );
    return $form;
  }

  $headers = [
    t('User'),
    t('IP'),
    t('Created'),
    t('Actions'),
  ];

  $rows = [];

  foreach ($response->result as $rule) {
    $username = cloudflare_bypass_parse_key($rule->notes)[2];
    $rule_id = $rule->id;
    $button_name = 'delete_' . $rule_id . '_' . $username;
    $form[$button_name] = [
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#name' => $button_name,
    ];

    $created_date = explode('T', $rule->created_on)[0];

    $row = [
      $username,
      $rule->configuration->value,
      $created_date,
      drupal_render($form[$button_name]),
    ];

    $rows[] = $row;
  }

  $form['bypass'] = [
    '#theme' => 'table',
    '#header' => $headers,
    '#tree' => TRUE,
    '#rows' => $rows,
  ];

  return $form;
}

/**
 * Implements hook_form_submit().
 */
function cloudflare_bypass_settings_form_submit($form, &$form_state) {
  if ($form_state['clicked_button']['#value'] = 'Delete') {
    $rule_id = explode('_', $form_state['clicked_button']['#name'])[1];
    $username = explode('_', $form_state['clicked_button']['#name'])[2];
    cloudflare_bypass_api_delete_rule($rule_id, $username);
  }
}
